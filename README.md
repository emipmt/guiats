# GUIATS

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

To deploy you need have repos guiats-prod, guiats-backend and guiats on same level directory
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
