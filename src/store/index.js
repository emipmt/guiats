import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userData: {},
    participant: {},
    printData: {}
  },
  mutations: {
    setUser(state, user) {
      state.userData = user
    },
    updateState(state, {
      prop,
      value
    }) {
      state[prop] = value
    }
  },
  actions: {},
  modules: {}
})