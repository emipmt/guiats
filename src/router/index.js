import Vue from 'vue'
import VueRouter from 'vue-router'

//Vistas general
import SignUp from "../views/SignUp"
import LogIn from "../views/LogIn"
import Restore from "../views/Restore"
import RestorePassword from "../views/RestorePassword"

//Vistas Admin
import Admin from "../views/admin/Admin"
import Clients from "../views/admin/Clients"
import PersonalDataOptions from "../views/admin/PersonalDataOptions"
import Aspects from "../views/admin/Aspects"
import Reactives from "../views/admin/Reactives"
import HelpClient from "../views/admin/HelpClient"
import HelpParticipant from "../views/admin/HelpParticipant"

//Vistas Cliente
import Client from "../views/client/Client"
import Home from "../views/client/Home"
import Monitoring from "../views/client/Monitoring"
import HelpClientRender from "../views/client/Help"
import Massive from "../views/client/Massive"
import MassivePersonalData from "../views/client/MassivePersonalData"
import massiveParticipantQuestionnaire from "../views/client/massiveParticipantQuestionnaire"
import PrintUsers from "../views/client/Print"


//Vistas del cuestionario
import Questionnaire from "../views/questionnaire/Questionnaire"
import LogInQuestionnaire from "../views/questionnaire/LogIn"
import HelpQuestionnaire from "../views/questionnaire/Help"
import PersonalData from "../views/questionnaire/PersonalData"
import ParticipantQuestionnaire from "../views/questionnaire/ParticipantQuestionnaire"

//result 
import Result from "../views/result/Result"




Vue.use(VueRouter)

const routes = [{
    path: '/',
    name: 'login',
    component: LogIn
  },
  {
    path: '/signup',
    name: 'signup',
    component: SignUp
  },
  {
    path: '/restore',
    name: 'restore',
    component: Restore
  },
  {
    path: '/restorepassword/:hash',
    name: 'restorepassword',
    component: RestorePassword
  },
  {
    path: '/print-users',
    name: 'print-users',
    component: PrintUsers
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    meta: {
      requiresAuth: true,
      admin: true
    },
    children: [{
        path: 'clients',
        name: 'clients',
        component: Clients,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },
      {
        path: 'personaldataoptions/:idUser/:orgName',
        name: 'personaldataoptions',
        component: PersonalDataOptions,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },
      {
        path: 'aspects',
        name: 'aspects',
        component: Aspects,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },
      {
        path: 'reactives',
        name: 'reactives',
        component: Reactives,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },
      {
        path: 'helpclient',
        name: 'helpclient',
        component: HelpClient,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },
      {
        path: 'helpparticipant',
        name: 'helpparticipant',
        component: HelpParticipant,
        meta: {
          requiresAuth: true,
          admin: true
        }
      },

    ]
  },
  {
    path: "/client",
    name: "client",
    component: Client,
    meta: {
      requiresAuth: true,
      admin: false
    },
    children: [{
        path: "home",
        name: "home",
        component: Home,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "monitoring",
        name: "monitoring",
        component: Monitoring,
        meta: {
          requiresAuth: true,
          admin: false
        }

      },
      {
        path: "help",
        name: "help",
        component: HelpClientRender,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "massive",
        name: "massive",
        component: Massive,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "massivepersonaldata",
        name: "massivepersonaldata",
        component: MassivePersonalData,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "massiveparticipantquest",
        name: "massiveparticipantquest",
        component: massiveParticipantQuestionnaire,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },

    ]


  },
  {
    path: "/questionnaire/:idUser",
    name: "questionnaire",
    component: Questionnaire,
    meta: {
      requiresAuth: false,
      admin: false
    },
    children: [{
        path: "login/:work?/:password?",
        name: "loginquestionnaire",
        component: LogInQuestionnaire,
        meta: {
          requiresAuth: false,
          admin: false
        }
      },
      {
        path: "personaldata",
        name: "personaldata",
        component: PersonalData,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "participantquestionnaire",
        name: "participantquestionnaire",
        component: ParticipantQuestionnaire,
        meta: {
          requiresAuth: true,
          admin: false
        }
      },
      {
        path: "help",
        name: "helpquestionnaire",
        component: HelpQuestionnaire,
        meta: {
          requiresAuth: true,
          admin: false
        }
      }
    ]
  },
  {
    path: "/result/:clientId/:participantId",
    name: "result",
    component: Result,
    meta: {
      requiresAuth: true,
      admin: false
    }
  },

]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {

  const requiresAuth = to.matched.some(route => route.meta.requiresAuth);
  const admin = to.matched.some(route => route.meta.admin);
  let user = JSON.parse(sessionStorage.getItem("user"))
  let participant = JSON.parse(sessionStorage.getItem("participant"))
  let toQuestionnaire = to.path.includes("questionnaire")

  if (requiresAuth) {
    if (user == null && !toQuestionnaire) {
      next("/")
    } else if (toQuestionnaire && participant == null) {
      next("/questionnaire/1/login")
    } else {
      //requiresAuth = true y el usuario existe
      if (admin) {
        //admin = true
        if (user.type && user.active) {
          next()
        } else {
          next("/client/home")
        }
      } else {
        next()
      }
    }
  } else {
    next()
  }

})

export default router